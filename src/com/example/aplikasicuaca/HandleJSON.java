package com.example.aplikasicuaca;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;


public class HandleJSON {

	private Integer temperature = 0;
	private String urlString = "null";
	private String icon = "icon";

	public volatile boolean parsingComplete = true;

	public HandleJSON(String url){
		this.urlString = url;
	}

 	public Integer getTemperature(){
 		temperature = temperature - 273;
 		return temperature;
	}

 	public String getIcon(){
 		return icon;
	}

 	public void readAndParseJSON(String in){
 		try{
 			JSONObject reader = new JSONObject(in);
 			JSONObject main = reader.getJSONObject("main");
 			
 			temperature = main.getInt("temp");
 			
 			JSONArray weather = reader.getJSONArray("weather");
 			JSONObject Weather = weather.getJSONObject(0);
 			
 			icon = Weather.getString("icon");
 			
 			parsingComplete = false;
 		}catch (Exception e){
 			e.printStackTrace();
 		}
 	}
 	
 	public void fetchJSON(){
 		Thread thread = new Thread(new Runnable(){
 			public void run(){
 				try{
 					URL url = new URL(urlString);
 					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
 					conn.setReadTimeout(10000);
 					conn.setConnectTimeout(15000);
 					conn.setRequestMethod("GET");
 					conn.setDoInput(true);
 					conn.connect();
 					
 					InputStream stream = conn.getInputStream();
 					String data = convertStreamToString(stream);
 					readAndParseJSON(data);
 					stream.close();
 				}catch(Exception e){
 					e.printStackTrace();
 				}
 			}
 		});
 		thread.start();
 	}
 	
 	private String convertStreamToString(java.io.InputStream is) {
 		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
 		return s.hasNext() ? s.next() : "";
 	}

}
